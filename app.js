const imgA = document.querySelector('.img-a');
const imgB = document.querySelector('.img-b');
const imgC = document.querySelector('.img-c');
const imgD = document.querySelector('.img-d');
const imgE = document.querySelector('.img-e');
const textA = document.querySelectorAll('.text-a');
const nav = document.querySelector('nav')

const tl = new TimelineMax();

tl.fromTo(imgC, 1.8, {x: -1000}, {x:0, ease: Power1.easeInOut})
.fromTo(imgB, 1.8, {y: -1000}, {y:0, ease: Power1.easeInOut}, "-=1")
.fromTo(imgA, 1.8, {x: -1000}, {x:0, ease: Power1.easeInOut}, "-=1")
.fromTo(imgD, 1.8, {y: -1000}, {y:0, ease: Power1.easeInOut}, "-=0.9")
.fromTo(imgE, 1.8, {x: 1100}, {x:0, ease: Power1.easeInOut}, "-=0.8")
.fromTo(nav, 1.8, {x: -500, opacity: 0}, {x: 0, opacity: 1, ease: Power2.easeInOut}, "-=0.5")
.fromTo(textA, 1.8, {opacity: 0}, {opacity: 1, ease: Power1.easeInOut}, "+=0.5")


new hoverEffect({

    parent: document.querySelector('.distortion'),
    intensity: 0.2,
    image1: './media/liquify/bw2.jpg',
    image2: './media/liquify/bw1.jpg',
    displacementImage: './media/liquify/diss.png',
    imagesRatio: 1920/1380

});